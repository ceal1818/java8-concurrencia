package edu.java8course.forkjoin;

import java.util.Random;
import java.util.concurrent.ForkJoinPool;

public class ForkJoinExample {

	public static void main(String[] args) {
		int [] values = new int[100];
		Random random = new Random();
		
		for (int i = 0; i < values.length; i++) {
			values[i] = random.nextInt(10000);
		}
		
		for (int i : values) {
			System.out.print(i+", ");
		}
		System.out.println();
		
		IncrementFork ifBase = new IncrementFork("ifBase", values, System.currentTimeMillis(), 0, values.length);
		ForkJoinPool fjPool = new ForkJoinPool();
		
		fjPool.execute(ifBase);
		
		do {
			System.out.println(fjPool.getActiveThreadCount());
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} while(!ifBase.isDone());
		
		fjPool.shutdown();
		
		if (ifBase.isCompletedNormally()) {
			Integer.toString(ifBase.getTotal());
			System.out.println("total: "+ifBase.getTotal());
		}
	}

}
