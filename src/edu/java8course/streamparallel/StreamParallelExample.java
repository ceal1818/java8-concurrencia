package edu.java8course.streamparallel;

import java.util.stream.IntStream;

public class StreamParallelExample {

	public static void main(String[] args) {
		IntStream intStream = IntStream.range(1, 10);
		
		long initialTime = System.currentTimeMillis();
		
		int total = intStream
				.map(StreamParallelExample::duplicar)
				.sum();
		
		System.out.println("1) MS: "+(System.currentTimeMillis()-initialTime));
		System.out.println("Total: "+total);
		
		initialTime = System.currentTimeMillis();
		
		IntStream intStream2 = IntStream.range(1, 10);
		
		total = intStream2
				.parallel()
				.map(StreamParallelExample::duplicar)
				.sum();
		
		System.out.println("2) MS: "+(System.currentTimeMillis()-initialTime));
		System.out.println("Total: "+total);		
	}

	private static int duplicar(int num) {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return num * 2;
	}
	
}
